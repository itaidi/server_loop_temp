import os
import bottle
from bottle import route, run, app, response
from bottle import request, HTTPResponse
from dotenv import load_dotenv
from datetime import datetime
#import LeadModuls
from BtcMethods.api_methods import BtcMethods

# "\"l\" is an invalid base58 encoded character. // не правильный кошелек
# Balance 12976 is less than 23971 (including fee). // не достаточно баланса
# "HTTP Error 400: Bad Request" // не правильный кошелек http баланс
#  "Code: -25, Error: bad-txns-inputs-missingorspent" // create_transaction // brc

# 📌
# если мало баланса - "Code: -26, Error: dust"
# если ок баланс  0.00001 - fee = 37 "Code: -25, Error: bad-txns-inputs-missingorspent"
# если fee = 300 -  "descritption": "Balance 73909 is less than 115900 (including fee)."
# fee = 150 "descritption": "Balance 34842 is less than 36250 (including fee)."
#
# 📌
# если дефолтный метод    "balance": 73909 "hash": "334b4f69093895f97ec12fc543be8bf0efb243f0cef9c8502010ba84403258e8"
# "unconfirmed_txrefs": 3
# Sent Value	0.00073909 Fee	0.00039066  +0.00000001 ฿T		
#   "unconfirmed_balance": -39067,   "final_balance": 34842,
# https://sochain.com/tx/BTCTEST/334b4f69093895f97ec12fc543be8bf0efb243f0cef9c8502010ba84403258e8
# 📌
# block io
# 0.00008360 BTCTEST, and a Block.io Fee of 0.00000000 BTCTEST.
# 📌📌📌
# https://sochain.com/api/ TESTNET
# Sent Value	0.00134842  Fee	0.00014171
# # Sent Value	0.00071196 Fee	0.00000235
# Sent Value	0.00069961 Fee	0.00000235 0.00069725

os.environ['TZ'] = 'Europe/Moscow'
private_key=os.getenv("private_key")
network=os.getenv("network") # settings 'TEST' or 'MAIN'

load_dotenv(verbose=True)
btcMethods = BtcMethods(private_key=private_key, network=network)#'cUPuEDZ8oWHsy6kA36Uj79yvXiKZDUPL3ZU422aAcwQQitj2rxQr',)
Application = app()



class EnableCors(object):
    name = 'enable_cors'
    api = 2

    def apply(self, fn, context):
        def _enable_cors(*args, **kwargs):
            # set CORS headers
            response.headers['Access-Control-Allow-Origin'] = '*'
            response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
            response.headers[
                'Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

            if bottle.request.method != 'OPTIONS':
                # actual request; reply with the actual response
                return fn(*args, **kwargs)

        return _enable_cors

def answer_or_error(code=None,desc=None,hash=None,balance=None,final_balance=None,
                    last_hash=None,amount=None, balance_before_sent=None):
    if(not code and desc and code!=0): code = 1 
    else: code = 0
    if(not desc): desc = 'ok!'
    out = {  "error": { "code":code, "descritption": desc  }, }
    if(hash): out['hash'] = hash
    if(last_hash): out['last_hash'] = last_hash
    if(balance or balance==0): out['balance'] = balance
    if(final_balance): out['final_balance'] = final_balance
    if(amount): out['amount'] = amount
    if(balance_before_sent): out['balance_before_sent'] = balance_before_sent
    return out

@Application.route('/api/create_wallet', method=["GET"])
def index():
    return btcMethods.genWalletOrGetBalance('') # empty gen wallet

@Application.route('/api/balance', method=["OPTIONS", "POST"])
def index():
    json = request.json
    print(json)
    selected_network = network if not "network" in json else json['network']
    try:
        return answer_or_error(balance= btcMethods.getBalance(json['address'],selected_network))#'n2xTn1X8tJsGyyP4n685qLsWUmxz8yfjDC')}#int(btcMethods.genWalletBalance('cUPuEDZ8oWHsy6kA36Uj79yvXiKZDUPL3ZU422aAcwQQitj2rxQr')) } # empty gen wallet
    except Exception as e:
        return answer_or_error(desc= str(e))

# чтобы получить баланс и данные по текущему кошельку (через встроеный приватный ключ)
@Application.route('/api/mywallet', method=["GET"])
def index():
    try:
        return {'balance':btcMethods.genWalletOrGetBalance(private_key)['balance']} # берет баланс по приватному ключу
    except Exception as e:
        return answer_or_error(desc= str(e))



@Application.route('/api/send', method=["OPTIONS", "POST"])
def index():
    data_json = request.json
    #print((5000/100000000)==0.00005)
    #else:
    #    print(False)
    print('/api/send')
    try:
        amount = 0
        if not 'to' in data_json: return answer_or_error(desc= "no 'to' address")
        if not 'amount' in data_json: return answer_or_error(desc= "no 'amount'")
        amount=data_json['amount']/100000000
        print("sent to "+data_json['to']+" "+str(amount))
        tx = btcMethods.sendBtc(    'from',
                                    data_json['to'],
                                    amount)#, method='default')  # дефолтный метод съедает комиссию
        #bal=btcMethods.genWalletOrGetBalance(private_key)
        return answer_or_error( 
            hash= tx['hash'], 
            desc = tx['result'], 
            last_hash=tx['last_hash'],
            code=tx['code'],
            amount="{:.8f}".format(tx['amount']),
            balance_before_sent=tx['balance_before_sent'],
            balance=btcMethods.genWalletOrGetBalance(private_key)['balance'],#bal['balance'],
            final_balance=None#bal['final_balance']
            )#'','tb1qazylgm8wshcqqz2qgmj30nyy4uw3sgfz43kedtfh7tfac5ynfcgqpfvz98',0.00005000) } # empty gen wallet
    except Exception as e:
        print('Error!')
        return answer_or_error(desc= str(e))


#print("my_balance: "+btcMethods.genWalletOrGetBalance(private_key)['balance'])
Application.install(EnableCors())

Application.run(host=os.getenv("server_ip"), port=os.getenv("server_port"), debug=False,reloader=True)


