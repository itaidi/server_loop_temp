

// берет свой кошелек, по приватному ключу, выдает в терминал информацию, выводит баланс
// приватный ключ прописан в .env
http://localhost:3000/api/mywallet

// создание кошелька
GET ✅
http://localhost:3000/api/create_wallet

// баланс по публичному ключу
POST ✅
http://localhost:3000/api/balance
{"address":"tb1qazylgm8wshcqqz2qgmj30nyy4uw3sgfz43kedtfh7tfac5ynfcgqpfvz98"}
{"address":"n2xTn1X8tJsGyyP4n685qLsWUmxz8yfjDC","network":"MAIN"}

// отправка транзакции
POST
http://localhost:3000/api/send
{
"from": "n2xTn1X8tJsGyyP4n685qLsWUmxz8yfjDC",
"to": "tb1qazylgm8wshcqqz2qgmj30nyy4uw3sgfz43kedtfh7tfac5ynfcgqpfvz98",
"amount" : 5000
}
