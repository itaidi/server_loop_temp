from typing import Dict, List
from bit import PrivateKeyTestnet, wif_to_key,network, Key, PrivateKey
import requests
import urllib
import json
import numpy as np
#from datetime import date,datetime
#from pytz import timezone
# hash decode
from blockcypher import decodetx


class BtcMethods:
    def __init__(self, private_key, network):
        self.private_key=private_key
        self.network = network

##############
    # берет кошелек или создает
    def getWalletByNet(self,wallet_priv_key,net):
        key = None
        if net == 'TEST':
            print("use TEST")
            if wallet_priv_key:
                print("wallet exitsts")
                key = PrivateKeyTestnet(wallet_priv_key)
            else:
                print("create wallet")
                key = PrivateKeyTestnet() # gen testNet key
        elif net == 'MAIN':
            print("use MAIN")
            if wallet_priv_key:
                print("wallet exitsts")
                #key = Key(wallet_priv_key)
                key = PrivateKey(wallet_priv_key)
            else:
                print("create wallet")
                #key = Key() # gen key
                key = PrivateKey() # gen key
        return key


##############
    def blockcypherNetPerfix(self,net=None):
        # TODO: входной аргемент Net
        selected_network = self.network if not net else net
        print("net blockcypher: "+ selected_network)
        return "main" if selected_network == "MAIN" else "test3"

    def sochainNetPerfix(self,net=None):
        # TODO: входной аргемент Net
        selected_network = self.network if not net else net
        print("net sochain: "+ selected_network)
        return "BTC" if selected_network == "MAIN" else "BTCTEST"
    # быстро
    # получение баланса через blockcypher в зависимости от сети
    def getBalanceHashByUrlAndNet(self,wallet,net=None,get_last_hash=None):#,net):

        u= 'https://api.blockcypher.com/v1/btc/'+self.blockcypherNetPerfix(net)+'/addrs/'+wallet
        print(u)
        r = json.load(urllib.request.urlopen(u))
        #r = requests.get(u)
        #print(r) # 📌 data recived
        #r.json()
        #print(r)
        # TODO: альтернативный способ взятия баланса если сервер недоступен
        #взять последгюю транзакцию
        if not get_last_hash: return {'balance':r['balance'],
                                      'final_balance':r['final_balance']}

        if 'unconfirmed_txrefs' in r:
            hashes=r['unconfirmed_txrefs']
            if len(hashes)>0: return hashes[-1]['tx_hash']#hashes[hashes.keys()[-1]]
        if 'txrefs' in r:
            hashes=r['txrefs']
            if len(hashes)>0: return hashes[-1]['tx_hash']#hashes[hashes.keys()[-1]]
        return 0
        

##############
    # медленно
    def getBalanceByBit(self,my_key):
        return my_key.get_balance()


##############
    def genWalletOrGetBalance(self,get_my_wallet_balance=None,ignore_get_balance=None):
        # TODO: выбор сети при получении баланса
        bit_key=self.getWalletByNet(get_my_wallet_balance,self.network) # по дефолту сеть из .env
        # info
        print(bit_key.version)
        private_key = bit_key.to_wif()
        public_address = bit_key.address
        print('Private Key: '+private_key) # priv key
        print('Address: '+public_address)
        #print(wif_to_key(bit_key.to_wif()))
        #print('fee:' + str(network.get_fee())) # отдельный запрос, имеет лимиты
        if(not ignore_get_balance):
            bal = self.getBalanceHashByUrlAndNet(public_address) # self.getBalanceByBit(bit_key)
            balance = bal['balance']
            final_balance = bal['final_balance']
            print('Balance: '+str(balance))
        else:
            balance = None
            final_balance = None
        #print('balance:' + my_key.get_balance())
        # getBalance (by privateKey)
        if get_my_wallet_balance:
            return {"bit_key": bit_key,"balance":balance,"final_balance":final_balance} # для удобства использования
        # genWallet / в случае генерации кошелька
        return {"private_key":bit_key.to_wif(), "address":public_address}

##############
    # дефолтный метод взятия баланса
    def getBalance(self,wallet,net=None):
        return self.getBalanceHashByUrlAndNet(wallet,net)['balance'] # final_balance

##############
    def sendTransaction(self,address,user_amount=None):
        # need to enter Value
        print('user_amount: '+str(user_amount))
        if user_amount:
            money = user_amount
        else:
            return {'result': "Not Allowed to Send Nothing",
                    'hash':None, 
                    'my_address':None,
                    'code':1, 
                    'amount':None,
                    'balance_before_sent':None}
        # Default Minimal send
            amount = 0
            money = amount + 0.00001 # если меньше 0.00001 Error: dust
            if self.network == 'TEST':
                money = amount + 0.00000001 # если меньше 0.00001 Error: dust

        #my_key = self.getWalletByNet(self.private_key, self.network)#PrivateKey(wif=BTC_KEY)
        my_key=self.genWalletOrGetBalance(self.private_key, ignore_get_balance=True)['bit_key']
        balance = my_key.get_balance(currency="satoshi")
        print(balance)
        print(my_key.address)

        unspents = my_key.get_unspents()
        print(unspents)
        fee = 37 # MAIN NET # 37 "Code: -25, Error: bad-txns-inputs-missingorspent"
        if self.network == 'TEST':
            fee = 1 #fee=37 # sochain.com 
            # Sent Value	0.00071196 Fee	0.00000235
            #fee = 150 # blockchain.info TESTNET #37 #34842 #39067 150 = "descritption": "Balance 34842 is less than 36250 (including fee)."
            #I think mem pool min fee not met means you have a fee below 1 sat per byte
        #try:
        #np.around(money, 6)
        tx_hash = my_key.create_transaction([(address, money, 'btc')], fee=fee, unspents=unspents)
        # convert hash
        tx_export = decodetx(tx_hash, api_key="1e3a8bd0368446269fdd40a3526686c0")
        tx_export = tx_export["hash"]
        print(tx_hash)
        print(tx_export)
        #except:
            #error_info = traceback.format_exc()
            #for id in admin_list["chat_id"]:
            #    bot.send_message(id, error_info)
        #    return money, False, None
        
        if self.network == 'TEST':
        # SOCHAN CON
            # меньше коммисия в тестовой сети
            url = 'https://sochain.com/api/v2/send_tx/'+self.sochainNetPerfix()+'/' #https://sochain.com/api/#networks-supported
            print('POST '+url+" ("+self.network+")")
            x = requests.post(url, data={
                        'tx_hex': tx_hash, 
                        'network':self.sochainNetPerfix()}) # sochain API # https://sochain.com/api/ 
            resp_json=json.loads(x.text)
            print('Info'+str(x.text))
            #print(json.loads(x.text))
            #print(resp_json['data']['txid'])
            if resp_json['status']=='success' and resp_json['data']['txid']:
                return {'result': x.text,
                        'hash':resp_json['data']['txid'], 
                        'my_address':my_key.address,
                        'code':0, 
                        'amount':money,
                        'balance_before_sent':balance}
            else:
                return {'result': x.text,
                        'hash':resp_json['data']['txid'], 
                        'my_address':my_key.address, 
                        'code':None,
                        'amount':money,
                        'balance_before_sent':balance}
        # BLOCKCHAIN_INFO
        else:
            url = 'https://blockchain.info/pushtx'
            print('POST '+url+" ("+self.network+")")
            x = requests.post(url, data={'tx': tx_hash})
            print('Info'+str(x.text))
            return {'result': x.text,
                    'hash':tx_export, 
                    'extended_hash':tx_hash, 
                    'my_address':my_key.address,
                    'code':0, 
                    'amount':money,
                    'balance_before_sent':balance}

        #print(tx_hash)
        #return {'result': x.text,'hash':tx_hash, 'my_address':my_key.address}
        
    def sendBtc(self,from_wallet,toAddress,amount=0,method=None):
        #transaction
        if(not method):
            r = self.sendTransaction(toAddress,amount)
            #default send
            #tx_hash = bit_key.send([(to, amount, 'btc')])
            #tx_hash.hash
            print("Hash:"+str(r['hash']))
            error = 0
            last_hash = 0
            try:
                print(r['my_address'])
                last_hash=self.getBalanceHashByUrlAndNet(r['my_address'], #toAddress,
                                                        get_last_hash=1)
                print('last_hash'+last_hash)
            except Exception as e:
                print(e)
                error = e
            r['last_hash']=last_hash
            return r
        elif method=="default":
            #bit_key = self.getWalletByNet(self.private_key, self.network)
            bit_key=self.genWalletOrGetBalance(self.private_key) #, ignore_get_balance=True)['bit_key']
            balance = bit_key['balance']
            bit_key = ['bit_key']
            tx_hash = bit_key.send([(toAddress, amount, 'btc')])
            return {'hash':tx_hash, 
                    'result':None,
                    'last_hash':None, 
                    'code':0, 
                    'amount':amount,
                    'balance_before_sent':balance }

""" my_key = PrivateKey(wif=BTC_KEY)
print(my_key.get_balance(currency="satoshi"))
print(my_key.address)
amount = 0
money = amount + 0.00001
unspents = my_key.get_unspents()
print(unspents)
fee = 37
try:
    tx_hash = my_key.create_transaction([(address, np.around(money, 6), 'btc')], fee=fee, unspents=unspents)
except:
    error_info = traceback.format_exc()
    for id in admin_list["chat_id"]:
        bot.send_message(id, error_info)
    return money, False, None
url = 'https://blockchain.info/pushtx'
x = requests.post(url, data={'tx': tx_hash})
result = x.text """