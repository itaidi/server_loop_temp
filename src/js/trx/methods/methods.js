//const fetch = require('node-fetch');
const request = require('request');

// Env config
const config = require('./../.env.json');//console.log(config.tronwebHeaders); //JSON.parse(fs.readFileSync('file', 'utf8'));
module.exports.config = config

// tronweb include
const TronWeb = require('tronweb')
const HttpProvider = TronWeb.providers.HttpProvider;

var fullNode,solidityNode,eventServer,privateKey_def,publicKey_def,tronWeb

// вынес
config.nodes={
    private: config.privateNode, // local
    testNet: "https://api.shasta.trongrid.io",
    mainNet: "https://api.trongrid.io",
};

// Select NET
if(config.useLocalPriateNode){// MAINNET
    console.log('Running PrivateNode ' + config.privateNode)
     fullNode = new HttpProvider(config.nodes.private);
     solidityNode = new HttpProvider(config.nodes.private);
     eventServer = new HttpProvider(config.nodes.private);
     privateKey_def = config.privateNode_privKey
     publicKey_def = config.privateNode_pubKey
}else if(config.usePublicTestNode){// TESTNET
    console.log('Running TESTNODE shasta.trongrid.io')
     fullNode = new HttpProvider(config.nodes.testNet);
     solidityNode = new HttpProvider(config.nodes.testNet);
     eventServer = new HttpProvider(config.nodes.testNet);
     privateKey_def = config.testNode_privKey
     publicKey_def = config.testNode_pubKey
}else if(config.usePublicNode){// MAINNET
    console.log("📌 Attention: Running on MAINNODE")
     fullNode = new HttpProvider(config.nodes.mainNet);
     solidityNode = new HttpProvider(config.nodes.mainNet);
     eventServer = new HttpProvider(config.nodes.mainNet);
     privateKey_def = config.mainNode_privKey
     publicKey_def = config.mainNode_pubKey
}
const CONTRACT = config.TRC_USDT_CONTRACT // usdtContract
//module.exports.privateKey_def
config.publicKey_def = publicKey_def
config.privateKey_def = privateKey_def
console.log("MyWallet: "+ publicKey_def)

function getTronWeb(){
    if(!tronWeb){
        tronWeb = new TronWeb(fullNode,solidityNode,eventServer,privateKey_def);
        if(!config.useLocalPriateNode) tronWeb.setHeader(config.tronwebHeaders);// headers
    }
    return tronWeb 
}
module.exports.getTronWeb = getTronWeb

//getTronWeb(); // init

async function checkTRCisvalid(hash,callback=null){
    //,"contractRet":"OUT_OF_ENERGY","contractData":{"data":"a9059cbb000000000000000000000000bfca862e4d7dfb82d4140dd2376ebb75e5341f760000000000000000000000000000000000000000000000000000000000000001","owner_address":"TGAA7MamLrDddjRhBGnHMdZJgfHapL5AKJ","contract_address":"TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t"},"data":"","cost":{"multi_sign_fee":0,"net_fee":0,"net_fee_cost":1000,

    let url = "https://apilist.tronscan.org/api/transaction-info?hash="+hash
    // fetch(url, { method: "Get" })
    //     .then(res => {res.json()})
    //     .then((json) => {
    //     // do something with JSON
    //         console.log()
    //     });
    let options = {json: true};
    request(url, options, (error, res, body) => {
        
        if (error) {
          console.log(error)
          return callback({status:0, error:error})
       };
    //    cost: {
    //     multi_sign_fee: 0,
    //     net_fee: 0,
    //     net_fee_cost: 1000,
    //     energy_usage: 0,
    //     fee: 0,
    //     energy_fee_cost: 280,
    //     energy_fee: 0,
    //     energy_usage_total: 0,
    //     origin_energy_usage: 0,
    //     net_usage: 345
    //   },

    //https://apilist.tronscan.org/api/transaction-info?hash=bffbd8027e4f8f96c1b4712f3e3568c6c5c66eb3230a0610d3db729db7d72cfb
    //https://apilist.tronscan.org/api/transaction-info?hash=b505fc7c1dd305ac56b1530bad7a1cfc8158e306a5cbceb63042c90e4b48e771
    //bffbd8027e4f8f96c1b4712f3e3568c6c5c66eb3230a0610d3db729db7d72cfb
    //b505fc7c1dd305ac56b1530bad7a1cfc8158e306a5cbceb63042c90e4b48e771

    if (!error && res.statusCode == 200) {
       
        console.log(body.contractRet )
        if(!body || !body.contractRet) return callback({status: 0,desc:"transaction not found"});
        const success=(body.confirmed && 
                        (body.contractRet.toLowerCase() == "SUCCESS".toLowerCase()))

        return callback({status: "ok",desc:body.contractRet});
        //return  console.log(body)
        //confirmed: true,
        //revert: false,
        //contractRet: 'OUT_OF_ENERGY',
      
        // do something with JSON, using the 'body' variable
    }else{
        return callback({status: 0,desc:body.contractRet});
    };
    });
}

//checkTRCisvalid("67afb0a475649e45e832d21230f381acacc2251fc4f31ce7224f9b1ceb666221",function(result){console.log(result)})

//async function test(){return await checkTRCisvalid("b505fc7c1dd305ac56b1530bad7a1cfc8158e306a5cbceb63042c90e4b48e771")}
//console.log(test())

////////////
module.exports.transferTRC = async function transferTRC(amount,toAccount,callback=null){//amount, fromAccount, toAccount, PrivateKey) {
    const tronWeb = getTronWeb()//new TronWeb(fullNode, solidityNode, eventServer, PrivateKey);
    // tronWeb.setHeader(config.tronwebHeaders);
    fromAccount = publicKey_def
try{
    const {
        abi
    } = await tronWeb.trx.getContract(CONTRACT);

    const contract = tronWeb.contract(abi.entrys, CONTRACT);

    // check balance trx
    const balance_trx=await getBalanceTrx(config.publicKey_def)
    console.log("balance TRX:", balance_trx.toString());
    if(balance_trx<5000000){
        console.log("No balance (trx) for fee")
        return "No balance (trx) for fee"
    }

    const balance = await contract.methods.balanceOf(fromAccount).call();
    console.log("balance USDTRC:", balance.toString());

    
    //const balance2 = await contract.methods.balanceOf(toAccount).call();
    //console.log("balance2:", balance2.toString());
    console.log(balance.toString(), amount)
    if (Number(balance.toString()) < amount + 0) {
        resp = "No balance USDT (TRC)"
        return resp
    }
    console.log(amount, toAccount)
    resp = await contract.methods.transfer(toAccount, amount).send();
    // даже если баланс не правильный ❗️
    // 📌 срабатывает, но транзакция не создается / hash возвращается, но не выдает ошибки
    // "Account resource insufficient error." = недостаточно чего-то
    
    console.log(resp)
    if(!resp) return ("TransferTRC error " + resp)
    // check
  

/* var count=0
    // повторный запрос
let timerId = setInterval(() =>     checkTRCisvalid(resp,(check)=>{

    console.log(check)
    count++
    if(count>10) {clearInterval(timerId); return callback( "Error tx not found (10 trys)") }

    //if(!check || !check.status) return callback("Transaction check error: "+resp.desc); // check transaction tr    
    return callback( {tx:resp})
 }), 1000); */


    
    return {tx:resp}
    //return resp
}catch (error) { 
    console.log(error)
    const err = (error.message ? error.message : (error.reason? error.reason :(error.error? error.error :error)))
    console.log('transferTRC() Failure:',err);
    return err.toString()
    return callback( err.toString())
} }

////////////
module.exports.balanceTRC = async function balanceTRC(address){//, privateKey) {
    const tronWeb = getTronWeb()//new TronWeb(fullNode, solidityNode, eventServer, privateKey)
    // tronWeb.setHeader(config.tronwebHeaders);
try{
    const {
        abi
    } = await tronWeb.trx.getContract(CONTRACT);

    const contract = tronWeb.contract(abi.entrys, CONTRACT);

    const balance = await contract.methods.balanceOf(address).call();
    console.log("balance:", balance.toString());
    resp = balance.toString()
    return resp
}catch (error) { 
    console.log('getBalance() Failure:',error);
    return error
} }

////////////
// just Trx
module.exports.getBalanceTrx =  getBalanceTrx = async (address) => {
    try {
        console.log('try getBalanceTrx')
        // simple address check
        if(!address || address.length<4){
            const error = "Wrong Address"
            console.log('getBalance() Failure: '+error)
            return error
        }
const Add = address;
const gBalance = await tronWeb.trx.getBalance(Add);
const gBandwidth = await tronWeb.trx.getBandwidth(Add);
const getAccount = await tronWeb.trx.getAccount(Add);
const getAccountResources = await tronWeb.trx.getAccountResources(Add);
console.log("getBalance : ", gBalance);
console.log("getBandwidth : ", gBandwidth);
console.log("getAccount : ", getAccount);
console.log("getAccountResources : ", getAccountResources);
console.log("   ", Add);

 // return balance
return gBalance

}catch (error) { 
    console.log('getBalance() Failure:',error);
    return error
}};



////////////
module.exports.createAccount = function createAccount(callback){

const account = TronWeb.createAccount(); console.log(account); 

account.then(function (account) { const isValid = tronWeb.isAddress(account.address['hex']);
console.log('- Private Key:', account.privateKey);
console.log('- Base58:', account.address.base58);
console.log('- Valid: ', isValid, '\n');
account.isValid = isValid;
       
callback(account) // send account to
//res.json(account); // отправка аккаунта
    //console.log(req.body);
    //res.send("Hello World !");
});

}

////////////
module.exports.sendTrx = async function sendTrx(amount,toAddress){
    try {
    const fromAddr = false || publicKey_def;
    console.log(fromAddr  +" "+ toAddress +" "+ amount)
    console.log('try send')
        //(to, amount, from, options);
    const tradeobj = await tronWeb.transactionBuilder.sendTrx(toAddress, amount,fromAddr,1);
    //console.log(privateKey_def)
    const signedtxn = await tronWeb.trx.sign(tradeobj, privateKey_def);
    const receipt = await tronWeb.trx.sendRawTransaction(signedtxn);

    return receipt // answer

}catch (error) { 
    console.log('Task sendTrx() Failure: ',error);
    return error
}};

////////////
module.exports.triggerSmartContract = async function triggerSmartContract() {

    console.log("triggerSmartContract")
    const trc20ContractAddress = CONTRACT//"TQQg4EL8o1BSeKJY4MJ8TB8XK7xufxFBvK";//contract address

    try {
        let contract = await tronWeb.contract().at(trc20ContractAddress);
        //Use call to execute a pure or view smart contract method.
        // These methods do not modify the blockchain, do not cost anything to execute and are also not broadcasted to the network.
        let result = await contract.name().call();
        console.log('result: ', result);
    } catch(error) {
        console.error("trigger smart contract error",error)
    }
}