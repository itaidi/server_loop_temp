// api part
const express = require("express");
const bodyParser = require('body-parser')
const fs = require("fs");

// trx eth btc
// POST http://localhost:3000/api/balance
// {"address":"TFjFaHBiVNpcBJoaQsyJ1WjGK7ZJVeqKNG"}
// GET http://localhost:3000/api/create_wallet
// POST http://localhost:3000/api/send 
// {"to":"TFjFaHBiVNpcBJoaQsyJ1WjGK7ZJVeqKNG", amount:1 } // минимальная единица
// GET http://localhost:3000/api/mywallet 

// 📌 для отправки usdt нужна инициализация кошелька, и на балансе минимум 3-5 trx
// trc
// POST http://localhost:3000/api/balance_usdt
// {"to":"TFjFaHBiVNpcBJoaQsyJ1WjGK7ZJVeqKNG", amount:1 } // минимальная единица
// GET http://localhost:3000/api/mywallet 

// load methods tronWeb
const methods = require("./methods/methods.js");//new methods(");
//console.log(methods.getTronWeb())
tronWeb = methods.getTronWeb(); // init
const config = methods.config //console.log(config.publicKey_def)

// express start
const app = express();
app.use(bodyParser.json())// json body

// default request
const func = require("./methods/func.js");

// создание кошелька
app.get("/api/create_wallet",function(req, res){ 
    methods.createAccount((account) => { 
            res.json(account); }) }); // answer 

    //    {
    //        “address”: “some trx wallet address”
    //        }

// баланс usdt
app.post("/api/balance_usdt",function(req, res){ console.log(req.body.address);
    methods.balanceTRC(req.body.address).then((answer) => {
        res.json(func.answer_balance(answer))  }) }) // answer 
// баланс
app.post("/api/balance",function(req, res){     console.log(req.body.address);
    methods.getBalanceTrx(req.body.address).then((answer) => {
        return res.json(func.answer_balance(answer))  }); });  // answer 

app.get("/api/mywallet",function(req, res){     console.log(config.publicKey_def)
    methods.getBalanceTrx(config.publicKey_def).then((b) => {
        const b_trx=func.answer_balance(b);//res.json({"balance":b}) 
        methods.balanceTRC(config.publicKey_def).then((b) => {
            const b_trc=func.answer_balance(b);//res.json({"balance":b}) 
            res.json({"trx":b_trx, "usdt":b_trc})
        }) }) });  // answer 

    //    {
    //        “currency”: “USDTTRC”,
    //        “address”: “some trx wallet address”
    //        }

app.post("/api/send_usdt",function(req, res){
    //amount, fromAccount, toAccount,
    console.log("send USDt "+ req.body.amount + " from "+ (req.body.from?req.body.from:config.publicKey_def)+" to "+req.body.to)
    methods.transferTRC(req.body.amount, req.body.to)//,function(answer){res.json(func.answer_tx(answer))  })
    .then((answer) => {
        return res.json(func.answer_tx(answer))  
    });
})



    //    {
    //        “to”: “some trx wallet address”
    //        "amount" : 1
    //        }

// отправка trx
app.post("/api/send",function(req, res){
    console.log('/api/send sendTrx')  
    methods.sendTrx(req.body.amount, req.body.to).then((answer)=>{
        console.log(answer)
        return res.json(func.answer_tx(answer))
    }) });


// попытка внестиконтракт
app.get("/api/test_init_contract",function(req, res){

    methods.triggerSmartContract() // test contract
    //triggerSmartContract();
    // Contract has not been deployed on the network

    //https://tronscan.org/#/contract/TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t/code
    let abi =  fs.readFileSync('trc20/trc20usdt_abi.json', 'utf8');//'{"test":"test"}';
    //console.log(JSON.parse(abi)[0]['constant'])
    let code = fs.readFileSync('trc20/trc20usdt_bytecode', 'utf8');//'bytecode';
    console.log(code)

    async function deploy_contract(){
        
        let contract_instance = await tronWeb.contract().new({
        abi:JSON.parse(abi),
        bytecode:code,
        feeLimit:1000000000,
        callValue:0,
        userFeePercentage:1,
        originEnergyLimit:10000000,
        //parameters:[para1,2,3,...] //"constructor needs 4 but 0 provided"
        // TODO: параметры
      });
      console.log(contract_instance.address);
      res.json({"contract_address":contract_instance.address}); 
    }
    deploy_contract()
})


//остановка сервера

const listmethods = [
    'http://localhost:3000/api/balance',
    'http://localhost:3000/api/create_wallet',
    'http://localhost:3000/api/send',
    'http://localhost:3000/api/mywallet',

    'http://localhost:3000/api/balance_usdt',
    'http://localhost:3000/api/mywallet',
]

app.listen(config.server_port, function(req,res){
    console.log(listmethods.join('\n').split(':3000').join(config.server_port));
});

