// api part
const express = require("express");
const bodyParser = require('body-parser')
//const fs = require("fs");

// eth
// http://localhost:3000/api/balance
// {"address":"TFjFaHBiVNpcBJoaQsyJ1WjGK7ZJVeqKNG"}
// http://localhost:3000/api/create_wallet
// http://localhost:3000/api/send
// http://localhost:3000/api/mywallet 

// 📌 для отправки usdt нужна инициализация кошелька, и на балансе минимум 3-5 trx
// erc20
// http://localhost:3000/api/balance_usdt
// http://localhost:3000/api/mywallet 

// Provided address // eth error balance

// load methods web3
const methods = require("./methods/methods.js");//new methods(");
//console.log(methods.getWeb3())
//console.log(methods.erc20.sendToken)
web3 = methods.getWeb3(); // init
const config = methods.config //console.log(config.publicKey_def)
const Tx = methods.Tx

// вывод списка аккаунтов при старте сервера
web3.eth.getAccounts().then(e => console.log(e));


// express api start
const app = express();
app.use(bodyParser.json())// json body

// default request
const func = require("./methods/func.js");


// создание кошелька
app.get("/api/create_wallet",function(req, res){
    res.json(methods.createWallet()) });

    //    {
    //        “address”: “some trx wallet address”
    //        }
// баланс
app.post("/api/balance",function(req, res){ // request
        const address = req.body.address; console.log(address);
        methods.balance(address,(answer) =>{
            res.json(func.answer_balance(answer)) }); });

app.post("/api/balance_usdt",function(req, res){ // request
        const address = req.body.address; console.log(address);
        methods.balanceUSDT(address).then((ans) =>{  
            res.json(func.answer_balance(ans))
        }) });

app.get("/api/mywallet",function(req, res){ // request
        const address = config.publicKey_def; console.log(address);
        var eth_balance = 0
        var usdt_balance = 0
        methods.balance(address,(ans) =>{ eth_balance=func.answer_balance(ans) 
            methods.balanceUSDT(address).then((ans) =>{ usdt_balance=func.answer_balance(ans) 
                res.json({eth:eth_balance, usdt:usdt_balance, })
            }) }) })
        

        //        }
        //        “to”: “some trx wallet address”
        //        "amount" : 1
        //        }
// отправка
app.post("/api/send",function(req, res){

        const amount = 1 // req.body.amount // request vars
        
        // fromAddr, toAddress, amountToSend, gasPrice, gasLimit, nonce, chainId, privateKeyBuffer
        methods.sendEth(
                amount,  
                req.body.to,  // request vars
                (answer) => { res.json(func.answer_tx(answer))
                }) });

// отправка
app.post("/api/send_usdt",function(req, res){

    // 0x2c6C7a60CcE23630B1ED38735caa02d5ad52A081 testaddress

    // fixed amount
    const amount = 1 // req.body.amount,  // request vars

//if(answer && answer.result && answer.transaction)
//tx=answer.transaction.txID // trx

// to, amount, callback
/*     methods.sendToken(
            req.body.to,  // request vars
            amount,
            (answer) => { res.json(func.answer_tx(answer))
            }) }); */
        methods.sendContract(
                amount,  
                req.body.to,  // request vars
                (answer) => { res.json(func.answer_tx(answer))
                }) });

// старт сервера

const listmethods = [
    'http://localhost:3000/api/balance',
    'http://localhost:3000/api/create_wallet',
    'http://localhost:3000/api/send',
    'http://localhost:3000/api/mywallet',

    'http://localhost:3000/api/balance_usdt',
    'http://localhost:3000/api/mywallet',
]

app.listen(config.server_port, function(req,res){
    console.log(listmethods.join('\n').split(':3000').join(config.server_port));
});















