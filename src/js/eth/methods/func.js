// default request
function my_answer(obj={}){  
    obj.desc = obj.desc || "Ok!"; 
    obj.error = obj.error || 0;
    const ans= {
        "error": {
        "code": obj.error,
        "descritption": obj.desc//"Ok!"
        },
        //"hash": obj.hash//"хэш транзакции"
    }
    if(obj.hash) ans.hash = obj.hash
    if(typeof obj.balance !== 'undefined') ans.balance = obj.balance
    return ans
}
module.exports.my_answer = my_answer

// json ответы response
module.exports.answer_balance = function answer_balance(answer){
    if(answer == Number(answer)) answer= Number(answer) // fix TRC
    if(answer && typeof answer == "string") 
        return my_answer({error:1, desc: answer})
    return {"balance":answer} }

module.exports.answer_tx = function answer_tx(answer){var tx
    if(answer && typeof answer == "string") 
    return my_answer({error:1, desc: answer})

    // balance is not sufficient // TRX SEND console
    // Account resource insufficient error // TRC SEND - недостаточно средств, или TRX для транзакции

    if(answer && answer.result && answer.transaction)
        tx=answer.transaction.txID // trx
    if(answer && answer.tx) tx=answer.tx // trc usdt
    if(tx) return my_answer({hash:tx}) // return hash
    return my_answer({error:2, desc: "other error"})
}