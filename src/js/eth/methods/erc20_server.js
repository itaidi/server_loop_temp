function lengthInUtf8Bytes(str) {
  // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
  var m = encodeURIComponent(str).match(/%[89ABab]/g);
  return str.length + (m ? m.length : 0);
}
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/v3/555593a12b3946328ab58bf4b485f9ae'));
var Tx = require("ethereumjs-tx").Transaction
const contractAddr = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
const contractAbi = [
    {
        "constant": true,
        "inputs": [],
        "name": "name",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_spender",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "approve",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "totalSupply",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_from",
                "type": "address"
            },
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transferFrom",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "decimals",
        "outputs": [
            {
                "name": "",
                "type": "uint8"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            }
        ],
        "name": "balanceOf",
        "outputs": [
            {
                "name": "balance",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "symbol",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_spender",
                "type": "address"
            }
        ],
        "name": "allowance",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "owner",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "spender",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Approval",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "from",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Transfer",
        "type": "event"
    }
];
const contractOwner = {
//	addr: '',
//	key: ''
};
// from config
const config = require('./../.env.json');
module.exports.config = config
contractOwner.addr = config.mainNode_pubKey
contractOwner.key = config.mainNode_privKey

const contract = new web3.eth.Contract(contractAbi,contractAddr);//web3.eth.contract(contractAbi).at(contractAddr);
async function getBalance(address) {
  console.log(address)
try{
  balance = await contract.methods.balanceOf(address).call();
  console.log("balance USDT_ERC20:" + balance)
  return Number(balance);
}catch (e) { 
    e = (e.message ? e.message : (e.reason? e.reason :(e.error? e.error :e)))
    console.log('Balance USDT_ERC20() Failure:',e);
    return(e.toString())
} }
module.exports.getBalance = getBalance
//getBalance(contractOwner.addr).then((b)=>console.log(b))

//sendToken('0x8B2Da8359BFeF3480cbedFC87A501Ce9D1097C73', '1');
module.exports.sendToken = function sendToken(receiver, amount,callback=null) {
	console.log(`Start to send ${amount} tokens to ${receiver}`);
try { // catch any error
	//const contract = web3.eth.contract(contractAbi).at(contractAddr);
    //const data = contract.transfer.getData(receiver, amount);
    console.log(contract)
	const data = contract.methods.transfer(receiver, amount);
    //console.log(data)
	const gasPrice = web3.eth.gasPrice;
	const gasLimit = 100000;
	gasPrice.c[0] = 1;
	console.log(gasPrice);

	var nonce =  web3.toHex(web3.eth.getTransactionCount(contractOwner.addr))
	const rawTransaction = {
		'from': contractOwner.addr,
		'nonce': nonce,
		'gasPrice': web3.toHex(gasPrice),
		'gasLimit': web3.toHex(gasLimit),
		'to': contractAddr,
		'value': 0,
		'data': data,
		'chainId': 1
	};
	console.log(lengthInUtf8Bytes(data) * 68 + 21000, " - DATA LENGTH");
	const privKey = new Buffer(contractOwner.key, 'hex');
	const tx = new Tx(rawTransaction);
	tx.sign(privKey);
	const serializedTx = tx.serialize();
	console.log((receiver))
	web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
		if (err) {
            callback(err.toString()) // error
			console.log(err);
		}
		console.log(hash);
        if(callback) callback({ result:1, transaction:{txID:hash}})
		return hash
	});
}catch (e) { 
    //console.log(e)
    e = (e.message ? e.message : (e.reason? e.reason :(e.error? e.error :e)))
    console.log('sendERC_contract() Failure:',e);
    callback(e.toString())
    //return err.toString()
} }