// Env config
const config = require('./../.env.json');//console.log(config.tronwebHeaders); //JSON.parse(fs.readFileSync('file', 'utf8'));
module.exports.config = config

const abi_usdt = require('./usdt/abi.json');
const contractAbi = abi_usdt.abi
//console.log(contractAbi)
const contractAddr = '0xdAC17F958D2ee523a2206206994597C13D831ec7';

//const erc20 = require("./erc20_server.js");
//module.exports.erc20 = erc20
//console.log (erc20.sendToken)
//config = erc20.config
//module.exports.config = config

// eth web3, serv
const Web3 = require('web3');
//web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/<account-id>'));
const Tx = require('ethereumjs-tx').Transaction;
const request = require('request');
//const ganache = require("ganache");
module.exports.Tx = Tx

function parseError(e){ 
   return (e.message ? e.message : (e.reason? e.reason :(e.error? e.error :e)))
}

var privateKey_def,publicKey_def,
    web3,eventServer,
    gasPrice, gasLimit, chainId, privateKeyBuffer

// вынес
config.nodes={
    private: config.privateNode, // local private hardhat
    localTestNet: config.localNodeGanache, // local ganache
    testNet: "", // web
    mainNet: "http://135.181.3.26:8545", // web MainNet // 'https://mainnet.infura.io/v3/555593a12b3946328ab58bf4b485f9ae'
};

// Select NET
if(config.useLocalPriateNode){// MAINNET hardhat
    console.log('Running PrivateNode hardhat ' + config.nodes.private)
     eventServer = config.nodes.private;
     privateKey_def = config.privateNode_privKey
     publicKey_def = config.privateNode_pubKey
}else if(config.useLocalTestNodeGanache){// TESTNET Ganache
    console.log('Running TESTNODE Ganache ' + config.nodes.localTestNet)
    eventServer = config.nodes.localTestNet;
     privateKey_def = config.testNodeGanache_privKey
     publicKey_def = config.testNodeGanache_pubKey
}else if(config.usePublicTestNode){// TESTNET WEB
    console.log('Running TESTNODE WEB-ERC')
     eventServer = config.nodes.testNet;
     privateKey_def = config.testNode_privKey
     publicKey_def = config.testNode_pubKey
}else if(config.usePublicNode){// MAINNET WEB
    console.log("📌 Attention: Running on MAINNODE WEB-ERC")
     eventServer = config.nodes.mainNet;
     privateKey_def = config.mainNode_privKey
     publicKey_def = config.mainNode_pubKey
}
const CONTRACT = config.ERC_USDT_CONTRACT // usdtContract
//module.exports.privateKey_def
config.publicKey_def = publicKey_def
config.privateKey_def = privateKey_def
console.log("MyWallet: "+ publicKey_def)

fromAddr = publicKey_def

function getWeb3(){
    if(!web3)
        web3 = new Web3(eventServer) // "http://127.0.0.1:7545"
    return web3 
} getWeb3()
module.exports.getWeb3 = getWeb3


// settings
var gasPrice = 2
var gasPriceNet = 0 // from net
var gasPriceTrue
//console.log("!"+gas)
web3.eth.getGasPrice().then(value => { gasPriceNet=value/1e8;  
    console.log("GasPrice:"+web3.utils.fromWei(value, 'ether'));  
    console.log("gasPrice: "+value/1e8);/* * 1e8 */     }, reason => {  });//2;//or get with web3.eth.gasPrice
var gasLimit = 100000;//3000000;
// 📌 Hardhat удалить 0x от приватного ключа
// 📌 MetaMask wallet для тестирования
var privateKeyBuffer = Buffer.from(config.privateKey_def, 'hex') // to sign
var chainId = 1337//5777; web3.eth.getChainId().then(console.log);


// chainId Get
web3.eth.getChainId().then(cid=>{ ////web3.eth.net.getId().then(e=>console.log("chainid: "+e));
    chainId = cid; // set
    console.log("chainid (on node): "+cid) });// get Chain ID
console.log('Buffer key: ', privateKeyBuffer)

/////////////
module.exports.createWallet = function createWallet(){
var accountsLen=1 // количество создаваемых аккаунтов
//const accounts = web3.eth.accounts.wallet.create(2, '54674321§3456764321§345674321§3453647544±±±§±±±!!!43534534534534');
accounts = web3.eth.accounts.wallet.create(accountsLen,web3.utils.randomHex(32));
//web3.eth.accounts.privateKeyToAccount('0x348ce564d427a3311b6536bbcff9390d69395b06ed6c486954e971d960fe8709');
console.log(accounts[0])
// /console.log(accounts[0].privateKey)
if(web3.eth.accounts.wallet.add(accounts[0])){
return accounts[0]; // coздание
//return delete(accounts[0]); // очистка массива
} }

/////////////
module.exports.balance = function balance(address,callback){
try {
    web3.eth.getBalance(address).then(b => {
        console.log('fromAddr balance: '+b)
        callback(b)
    })
}catch (error) { 
    console.log(error)
    const err = parseError(error)
    console.log('eth balance() Failure:',err);
    callback(err.toString())
} }



function lengthInUtf8Bytes(str) {
    // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
    var m = encodeURIComponent(str).match(/%[89ABab]/g);
    return str.length + (m ? m.length : 0);
}


const contract = new web3.eth.Contract(contractAbi,contractAddr);//web3.eth.contract(contractAbi).at(contractAddr);


/////////////
function sendEth(amountToSend,toAddress,callback,isContract=null){//fromAddr, toAddress, amountToSend//, gasPrice, gasLimit, chainId, privateKeyBuffer){

        // error 
        if(!toAddress || toAddress.length< 10) 
            return callback("Wrong Adress")
        if(!amountToSend) 
            return callback("Wrong amount")

const send = function(fromAddr,toAddress,amountToSend,gasPrice,gasLimit,nonce,chainId,isContract=null){



    console.log('Gas  price: ', gasPrice)
    console.log('gas Limit: ', gasLimit)
        var data =0
    if(isContract) { // contract
        //data = contract.methods.transfer(receiver, amount);
        console.log("SEND CONTRACT()");
        data = contract.methods.transfer(toAddress, amountToSend).encodeABI();
        console.log(data)
        if(!data){
            console.log("contract error")
            callback("contract error")
        }
    }


    var rawTransaction = {
        "from": fromAddr,
        "nonce": web3.utils.toHex(nonce),
        "gasPrice": web3.utils.toHex(Math.ceil(gasPrice)),// * 1e8), //* 1e8,
    //Error: Error: [number-to-bn] while converting number 948.71164241 to BN.js instance, error: invalid number value. Value must be an integer, hex string, BN or BigNumber instance. Note, decimals are not supported. 
        "gasLimit": web3.utils.toHex(gasLimit),
        "to": toAddress,
        //"value": amountToSend ,
        "chainId": chainId //remember to change this
    };
    if(isContract) {
        // send contract
        rawTransaction = Object.assign(rawTransaction,{'to': contractAddr,
		'value': 0, //  для контракта 0
		'data': data,})
    }else{
        rawTransaction.value = amountToSend // send eth
    }
    //return 0;
    console.log(rawTransaction);

    // создание подписи
    const tx = new Tx(rawTransaction); tx.sign(privateKeyBuffer);
    const serializedTx = `0x${tx.serialize().toString('hex')}`;


    web3.eth.sendSignedTransaction(serializedTx,  function(err, hash) {
        if (!err) {
            console.log('Txn Sent and hash is '+hash);
            //answer_base.hash = hash; // запись hash
            //res.json(answer_base); // вывод
            callback({tx:hash})
        } else {
            console.error(err); 
            err = parseError(err)
            callback(err.toString())
        } });
    }

try {
// получение номера транзакции и отправка

web3.eth.getTransactionCount(fromAddr).then(nonce => {//console.log(nonce);
    // после загрузки поличества тразакций
    // берем gasPrice
    web3.eth.getGasPrice().then(value_web3 => { 

        //https://ethgasstation.info/json/ethgasAPI.json
        //req = requests.get('https://ethgasstation.info/json/ethgasAPI.json')
        //t = json.loads(req.content)
        //print('safeLow', t['safeLow'])
        //print('average', t['average'])
        //print('fast', t['fast'])
        //print('fastest', t['fastest'])

        gasPriceNet=value_web3/1e8;  
        gasPriceTrue = web3.utils.fromWei(value_web3, 'ether')
        console.log("GasPrice Wei:"+gasPriceTrue);  
        console.log("gasPrice: "+gasPriceNet);/* * 1e8 */    


        let options = {json: true};
        let url ='https://ethgasstation.info/json/ethgasAPI.json' // get gas price
        request(url, options, (error, res, ethgasstation_body) => {
            if (error) { console.log(error); return callback(error)};
            if (!error && res.statusCode == 200) { // ok
        //console.log(body)
        console.log('safeLow', ethgasstation_body['safeLow'])
        console.log('average', ethgasstation_body['average'])
        console.log('fast', ethgasstation_body['fast'])
        console.log('fastest', ethgasstation_body['fastest'])

        //safe_gas= ethgasstation_body['safeLow']
        //net_gas = gasPriceOk
        //console.log("GasPrice save Wei: "+web3.utils.fromWei(""+ethgasstation_body['safeLow']));  
        //console.log(body.contractRet )
        //if(!body || !body.contractRet) return callback({status: 0,desc:"transaction not found"});
                
        
        console.log( Math.min(10, 20))
        var minimal = Math.min(ethgasstation_body['safeLow'], value_web3) // ethgasstation vs web3
        console.log("minimal: "+ minimal)
        //var safe_gas_valid = web3.utils.fromWei(""+body['safeLow'])
        //var gasPriceOk=web3.utils.fromWei(""+value); 

        // format
        if(minimal >1500) return callback("Danger - wrong etc Gas: "+ minimal)
        var safe_gas_valid_minimal = web3.utils.fromWei(""+minimal)
        /// return 0 
        
    //return 0;
    //console.log(safe_gas_valid)

    send(fromAddr, toAddress, amountToSend,  safe_gas_valid_minimal/* 📌 */, gasLimit, nonce, chainId, isContract);
    
    

    // error load from site
    }else{         return callback("Error load gas price");     }; });
        
    // error load gasPrice web3
    }, reason => { console.log('sendEth() Failure:',reason);callback(reason) });//2;//or get with web3.eth.gasPrice

// get nonce
});  
// other error
}catch (error) { 
    console.log(error)
    const err = parseError(error)
    console.log('sendEth() Failure:',err);
    callback(err.toString())
    //return err.toString()
}

}
module.exports.sendEth = sendEth;
module.exports.sendContract = function(toAddress,amount,callback){
    sendEth(toAddress,amount,callback,true /* isContract */);
}

async function balanceUSDT(address) {
  console.log(address)
try{
  balance = await contract.methods.balanceOf(address).call();
  console.log("balance USDT_ERC20:" + balance)
  return Number(balance);
}catch (e) { 
    e = (e.message ? e.message : (e.reason? e.reason :(e.error? e.error :e)))
    console.log('Balance USDT_ERC20() Failure:',e);
    return(e.toString())
} }
module.exports.balanceUSDT = balanceUSDT

/////////////
// module.exports.sendToken = function sendToken(receiver, amount,callback=null) {
// 	console.log(`Start to send ${amount} tokens to ${receiver}`);
// try { // catch any error
// 	//const contract = web3.eth.contract(contractAbi).at(contractAddr);
//     //const data = contract.transfer.getData(receiver, amount);
//     console.log(contract)
// 	const data = contract.methods.transfer(receiver, amount);
//     //console.log(data)
// 	//const gasPrice = web3.eth.gasPrice;
// 	//const gasLimit = 100000;
// 	//gasPrice.c[0] = 1;
//     console.log('Gas  price: ', gasPrice)
//     console.log('gas Limit: ', gasLimit)

// 	//var nonce =  web3.toHex(web3.eth.getTransactionCount(contractOwner.addr))
// 	const rawTransaction = {
// 		'from': contractOwner.addr,
// 		'nonce': nonce,
// 		'gasPrice': web3.toHex(gasPrice),
// 		'gasLimit': web3.toHex(gasLimit),
// 		'to': contractAddr,
// 		'value': 0, //  для контракта 0
// 		'data': data,
// 		'chainId': chainId//1
// 	};
// 	console.log(lengthInUtf8Bytes(data) * 68 + 21000, " - DATA LENGTH");
// 	//const privKey = new Buffer(contractOwner.key, 'hex');
// 	const tx = new Tx(rawTransaction); 	tx.sign(privateKeyBuffer);
// 	//const serializedTx = tx.serialize(); 
//     const serializedTx = `0x${tx.serialize().toString('hex')}`;
// 	console.log((receiver))
// 	web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
// 		if (err) {
//             callback(err.toString()) // error
// 			console.log(err);
// 		}
// 		console.log(hash);
//         if(callback) callback({ result:1, transaction:{txID:hash}})
// 		return hash
// 	});
// }catch (e) { 
//     //console.log(e)
//     e = parseError(e)
//     console.log('sendERC_contract() Failure:',e);
//     callback(e.toString())
//     //return err.toString()
// } }