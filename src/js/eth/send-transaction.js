var Web3 = require('web3');
var Tx = require('ethereumjs-tx').Transaction;

//web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/<account-id>'));

const web3 = new Web3('http://127.0.0.1:7545')

var gasPrice = 2
web3.eth.getGasPrice().then(value => {
    console.log(value);// * 1e8
    }, reason => {  });//2;//or get with web3.eth.gasPrice
const gasLimit = 3000000;

const fromAddr = "0x1367347D8A98C901Cd501700EFBF7C71FD3EE8eE";
const toAddress = "0x09a9f63502F8908B7dB62C3fdbd644233172a164"//"0x6616E92D0CDf1f6fC90Ea91f71aA27eBe48e83E0"
const privateKey = "aebf08fbce2bdc3b9b90e08833e1981e53578104e0bee3bd5f754fa172527a00"
const privateKeyBuffer = Buffer.from(privateKey, 'hex') // to sign
const chainId=5777; //web3.eth.getChainId().then(console.log);

web3.eth.getBalance(fromAddr).then(b => {console.log('fromAddr balance: '+b)})
web3.eth.getBalance(toAddress).then(b => {console.log('toAddress balance: '+b)})

// 
var amountToSend = (gasPrice * 1e8 + 1) //"0.00192823123348952"; //$1

console.log('Buffer key: ', privateKeyBuffer)

function send(fromAddr,toAddress,amountToSend,gasPrice,gasLimit,nonce,chainId){

    console.log('Gas  price: ', gasPrice)
    console.log('gas Limit: ', gasLimit)
    
    var rawTransaction = {
        "from": fromAddr,
        "nonce": web3.utils.toHex(nonce),
        "gasPrice": web3.utils.toHex(gasPrice * 1e8), //* 1e8,
        "gasLimit": web3.utils.toHex(gasLimit),
        "to": toAddress,
        "value": amountToSend ,
        "chainId": chainId //remember to change this
      };

      console.log(rawTransaction);
      
          //const tx = new Tx(rawTransaction);

    //var privKey = new Buffer(privateKey, 'hex');
    //var tx = new Tx(rawTransaction);
    //tx.sign(privKey);
    //var serializedTx = tx.serialize();

    const tx = new Tx(rawTransaction); tx.sign(privateKeyBuffer);
    const serializedTx = `0x${tx.serialize().toString('hex')}`;

    //web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), 
    
    web3.eth.sendSignedTransaction(serializedTx,  function(err, hash) {
      if (!err)
          {
            console.log('Txn Sent and hash is '+hash);
          }
      else
          {
            console.error(err);
          }
    });
}


web3.eth.getTransactionCount(fromAddr).then(nonce => {
    //console.log(nonce);

    // после загрузки поличества тразакций
    send(fromAddr, toAddress, amountToSend, gasPrice, gasLimit, nonce, chainId);

}); //211;



//console.log("amountToSend:" + amountToSend);
//console.log("gasPrice:" + gasPrice)
//console.log("nonce:" + nonce)











