<?php $start = microtime(true);set_time_limit(0);/*time off php*/;/*error_reporting(E_ALL ^ E_WARNING);*/define('TZ','Europe/Moscow');define('ONE_SECOND',1000000);//for pasue
date_default_timezone_set( TZ );$dt_tz = new DateTimeZone( TZ );//date_default_timezone_get();

// time
function timestamp_by_def_TZ($return_time=null){global $dt_tz;     
    $dt= new DateTime("now", $dt_tz );
    if($return_time == 'logs') return $dt->format('Y-m-d');
    if($return_time) return ($dt->getTimestamp() + $dt->getOffset());
    return $dt->format('Y-m-d H:i:s');} // to pg

echo 'Check' . ( microtime(true) - $start ) . ' sec.';
// 📌 Защита от запуска нескольких серверов
define("MYID",timestamp_by_def_TZ()."_".time());//if(file_exists('_start_server')){ echo('sorry server already started');exit;}
file_put_contents('_start_server',MYID); // запись id

// logger
//https://github.com/Seldaek/monolog
require_once '../vendor/autoload.php';
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
$log = new Logger('mylog');
$log->pushHandler(new StreamHandler('logs/'.timestamp_by_def_TZ('logs').'.log', Logger::WARNING));
function log_echo($info){ global $log;  echo($info);flush();   $log->warning($info);}

log_echo('NEW Script '.MYID.': ' . ( microtime(true) - $start ) . ' sec.');

// chmode settings / docker USER www

// nginx settings = 5years
//fastcgi_read_timeout 160704000;
//proxy_connect_timeout 160704000;
//proxy_send_timeout 160704000;
//proxy_read_timeout 160704000;
//send_timeout 160704000;

// нагрузка
// 1) requests url
// 2) UPDATE (status)
// 3) SELECT

// settings
define('REQUESTS_PAUSE', 1); // PAUSE before next requests in sec
define('ROWS_IN_ONE_SQL_SELECTION', 1000); // how many rows in one block
define('PAUSE_BEFORE_NEXT_SELECT', 3); // 5 secs
define('IS_LOOP', 1); // looped in prod
define('TOTAL_LIMIT', 1000); // 0 - нет лимита

$wallet_types=[
    #"ETH"=>['api'=>"http://web3eth_api:3000/api/send",],//"http://web3eth_api:3000/api/send",],     
    #"BTC"=>['api'=>"http://bitbtc_api:3000/api/send"],     
    //"USDT"=>['api'=>"http://tronweb_api:3000/api/send_usdt"],
];

// error statuses
$status_list=[
    1=>"Wallet not valid",
    2=>"Drection Not Valid",
    3=>"Direction Not supported",
    4=>"SendError",
    5=>"Write Db Error"
];

//host, hostaddr, port, dbname
//user, password, connect_timeout, options, tty

// Подготовка запроса
//$result = pg_prepare($dbconn, "my_query", 'SELECT * FROM form_lead WHERE name = $1');
// Запуск запроса на выполнение. Стоит отметить, что нет необходимости экранировать
// спецсимволы в строке "Joe's Widgets"
//$result = pg_execute($dbconn, "my_query", array("Joe's Widgets"));



// echo timestamp_by_def_TZ();exit;// check https://www.epochconverter.com/
$db_connect = "";
@require_once "_settings.php";

function get_conn($sql=null,$vars=null){global $db;global $db_connect;
    if(!$db) $db = pg_connect($db_connect);
    return $db;
}

// для одинарных запросов с подготовкой
function sql_driver($sql=null,$vars=[],$query_name=null){ $db = get_conn();
    if(!$query_name) $query_name='my_query'.timestamp_by_def_TZ(true)."_".time()."_".rand(0,10000); // random query name //var_dump($query_name);
    if(!$sql) $sql='SELECT * FROM information_schema.tables;';
    $r = pg_prepare($db, $query_name, $sql);
    $r = pg_execute($db, $query_name, $vars);//array("Joe's Widgets"));
    if(gettype($r)!="boolean") log_echo(json_encode(pg_fetch_all($r),JSON_UNESCAPED_UNICODE));
    return $r;
}
// для выполнения списокв запросов
function sql_query($sql_block){ if(!$sql_block||strlen($sql_block)<5) return 0; $db = get_conn(); $r = pg_query($db, $sql_block); 
    var_dump(json_encode(pg_fetch_all($r),JSON_UNESCAPED_UNICODE)); return $r; }

function add_colomn($table=null,$colomn=null,$type=null,){
    $r = sql_driver("ALTER TABLE ".$table." 
    ADD COLUMN ".$colomn." ".$type.";");
}

function send_json($url,$json){
    var_dump($url);
    var_dump($json);
    $body=json_encode($json);
    var_dump($body);
    $options = array(
        'http' => array(
          'method'  => 'POST',
          'content' => $body,
          'header'=>  "Content-Type: application/json\r\n" .
                      "Accept: application/json\r\n",
        //          "Authorization: Basic ".base64_encode("$https_user:$https_password")."\r\n",
        'content' => $body,
        'timeout' => 60,
        ),
      );
      
    $context  = stream_context_create( $options );
    $result = file_get_contents( $url, false, $context );
    log_echo(json_encode($result)); // save log
    return json_decode( $result ,JSON_OBJECT_AS_ARRAY);
}

// var_dump(json_encode(
//     send_json(
//         "http://temp_backend:3000/api/balance",
//         ["address"=>"mjwXUpaPoNXbrpnh3iCTo713xTjhPUXjU1",
//         "network"=>"TEST"])));

// if not exists
@add_colomn('lead_table','wallet_init',"timestamp"); // create colomn if not exists
@add_colomn('lead_table','wallet_init_error',"smallint"); // create colomn if not exists
@add_colomn('lead_table','init_hash',"VARCHAR(128)"); // create colomn if not exists
@add_colomn('lead_table','wallet_init_error_text',"VARCHAR(300)"); 

// SET
// $r = sql_driver('update lead_table SET wallet_init=$2 WHERE id=$1 returning id;',[
//     2,
//     timestamp_by_def_TZ(),]);
//$r = sql_driver('update lead_table SET wallet_init=null WHERE id=$1 returning id;',[
//        2,]);

////////////////////////////
// запись в бд флага
function wallet_sent_confirmed_to_db($id=null,$hash=null){
    if(!$id) return 0;
     $r = sql_driver('update lead_table SET wallet_init=$2, init_hash=$3 WHERE id=$1 returning id;',[
     $id,
     timestamp_by_def_TZ(),
     $hash,]);
     return $r;
}
////////////////////////////
function clean_init(){
    $r = sql_driver('update lead_table SET wallet_init=NULL WHERE id<>0;',[]);
} //clean_init();
///

function parseDirection($direction){
    if(!$direction) return 0;
    return trim(explode("->",$direction)[1]," "); // right part // string(10) "ETH -> RUR" 
}

function selectWalletApi($direction, $wallet_is_eth=null){ global $wallet_types; 
        if($wallet_is_eth){ // for USDT
            $perfix="ERC";
        }else{
            $perfix="TRC";
        }
        foreach ($wallet_types as $w_key => $w_api) {
        //var_dump($direction."==".$w_key."(".(strtolower($w_key) == strtolower($direction)).")");
        if(#strtolower($w_key) == strtolower($direction) || 
        strtolower($w_key) == strtolower($direction.$perfix)) // USDTRC USDERC
        return $w_api; }; }; // return api url 

// проверка валидности
function check_or_send_or_error($row){
    $api_url=null;
    $dir = $row['direction'];
    $wallet = $row['wallet'];
    $wallet=str_replace('
', '', $wallet);
    $wallet=implode("",explode("\\n",$wallet));
    $wallet=str_replace('\n', '', $wallet);
    $wallet=str_replace(' ', '', $wallet);
    
    echo "📌 wallet fix: ".$wallet;
    
    // 📌 1 check Wallet not valid
    //var_dump("📌".strlen($wallet)); // проверка кошелька
    if(!$wallet || strlen($wallet)<7) return 1;  // break
    if($dir) $dir=parseDirection($row['direction']); // get left part // string(10) "ETH -> RUR"

    //echo ('\n<br>'.$dir);

    // 📌 2 check Drection Not Valid 
    if(!$dir) return 2;  // break

    $wallet_is_eth=(stripos($wallet,"Ox",true) !==false);


    if($dir) $api_url=selectWalletApi($dir,$wallet_is_eth); 

    
    // 📌 3 Direction Not supported / no api found
    if(!$api_url) return 3; // break


    // send
    $url = $api_url['api'];
    log_echo("try send (".$api_url.")".$row['id']." ".$row['direction']." to ".$wallet." by api ".$url); // log

    $amount = 1;
    if($api_url['amount']) $amount = $api_url['amount'];
    $result = send_json(
        $url,
        ["to"=>$wallet,"amount"=>$amount]);
    
    // 📌 запись в базу данных если все ок
    //log_echo(json_encode($result)); // save to log answer
    if(!$result || $result['error']['code']==1) return 4; // send error
    //return 0;
    if($row['id'] && $result && !$result['error']['code'] && $result['hash']){
        $result= wallet_sent_confirmed_to_db($row['id'],$result['hash']);
        if(!$result) return 5; // write db error
        log_echo("- db ok ✅");
        return 0; // no  error
    }

     return 6; // somthig wrong
    

    
    #var_dump($result);

    // 📌 4 SendError
    #if(!$result) return 4; // break
    #return $result;
}

function check_mirror_exit(){
    if(file_get_contents('_start_server')!=MYID){ 
        log_echo("/n<br> 📌 ANOTHER SERVER STARTED /n<br>"); 
        query_error_sql_block(); // update wallet_init_errors && clean // выгрузка и выход
        exit;}
}



// запись флага ошибки в базу (в виде блоков)
$errors_sql_block=[];
function error_to_db($obj){$status=(int)$obj['status'];$id=$obj['id'];global $status_list;global $errors_sql_block;
    @log_echo('error status: '.$status.' id: '.$obj['id'].' info: '.$status_list[$status]);
    // записать статус
    //return 0;
    if($status && $status<4 && $id) 
    //$r = sql_driver(
    //'update lead_table SET wallet_init_error=$2 WHERE id=$1 returning id;',
    //[ $id,   $status,]); // 1 2
    $errors_sql_block[]=<<<EOT
update lead_table SET wallet_init_error=$status WHERE id=$id returning id;
EOT; #@log_echo($errors_sql_block[count($errors_sql_block)-1]);
}

function query_error_sql_block(){global $errors_sql_block;
    $sql_block=implode("
",$errors_sql_block);echo $sql_block;sql_query($sql_block);$errors_sql_block=[]; // update wallet_init_errors && clean
}

function get_rows_and_pauses_for_requests($how_many=1000,$request_pause_secs=0){global $start;

echo '$how_many:'.$how_many;
// GET
//echo('CheckBeforeSql: ' . ( microtime(true) - $start ) . ' sec.');
$r = sql_driver("SELECT direction, wallet, wallet_init, wallet_init_error, id, init_hash
                from lead_table 
                WHERE wallet_init is NULL and (wallet_init_error is NULL or wallet_init_error>6)
                LIMIT ".$how_many.";");

//echo('CheckAfterSql: ' . ( microtime(true) - $start ) . ' sec.');
//$r = sql_driver('SELECT * from lead_table WHERE wallet_init is not null LIMIT 1;');
//$r = sql_driver('SELECT * from lead_table WHERE id = 2;');


$list=pg_fetch_all($r);

// loop
$errors=[];$total=count($list);
foreach ($list as $n => $row) {
    //var_dump($row['direction']);
    // проверка валидности
    $status = check_or_send_or_error($row); // if ok
    // проверка статуса
    if(gettype($status) == "integer"){
        if(!isset($errors[$status])) $errors[$status]=0;$errors[$status]++;
        //echo('error-'.$status);
        error_to_db(['status'=>$status, 'id'=>$row['id']]); // save error to db
    }else if($status['status'] == "ok"){
        // flag to db
    }
    flush(); // вывод в output
    // 📌 защита от повторного запуска скрипта
    check_mirror_exit();
    /////////////////////

    if($request_pause_secs) usleep(ONE_SECOND * $request_pause_secs); // pause
}

return ['errors'=>$errors,'total'=>$total]; }
//////////////////////////////

log_echo('CheckBeforeWhile: ' . ( microtime(true) - $start ) . ' sec.');

$errors=[];$total_errors=0;$total=0;
// run loop if TRUE
while(1){

echo( 'CheckBeforeBlock: ' . ( microtime(true) - $start ) . ' sec.');
#log_echo(  ( microtime(true) - $start ) );



$e=get_rows_and_pauses_for_requests(
    ROWS_IN_ONE_SQL_SELECTION /*1000 rows*/, 
    REQUESTS_PAUSE/* one sec */);//var_dump($e);
    log_echo(  ( microtime(true) - $start ).' 📌 total_rows: '.$e['total'] );
    #log_echo('📌 total_rows: '.$e['total']);

if($e['total']>0){
// error logs
$total+=$e['total'];$e=$e['errors'];foreach($e as $k=>$v){
    if(!isset($errors[$k])) $errors[$k]=0;$errors[$k]+=$v;$total_errors+=$v;} // count errors
log_echo(json_encode(["total_rows"=>$total,"total_errors"=>$total,"errors"=>$errors])); 
}

// test exit
check_mirror_exit();
/////////////////////

if(count($errors_sql_block)>0){
// для ускорения объединение запросов
log_echo( '📌 update block of error statuses ');log_echo(json_encode($errors_sql_block));
query_error_sql_block(); // update wallet_init_errors && clean
}

echo( 'CheckAfterBlock: ' . ( microtime(true) - $start ) . ' sec.');

flush(); // вывод в output
// stop or loop
if(!IS_LOOP) exit; // exit if once run
if(TOTAL_LIMIT && $total>=TOTAL_LIMIT) exit; // exit on total limit
if(PAUSE_BEFORE_NEXT_SELECT) 
    usleep(ONE_SECOND * PAUSE_BEFORE_NEXT_SELECT); // pause if loop

}

// EXIT

?>
