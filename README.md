
#loop_sender

📌 Защита от запуска нескольких скриптов
📌 отключения таймаутов nginx, php
📌 возможны паузы (в настройках)
📌 защита от повторной отправки (проверка записи отправкой повторно)
📌 проверка времени выполнения
📌 логи в файл /src/php/loop_sender/public/logs по дню



права доступа на файл на запись (Dockerfile php) - USER www (важно)
root прописан в nginx.conf / для laravel /public

# порядок запуска

1) ganache (не использовать порт 8545) 📌 Error: Returned error: Invalid signature v value
2) run ganache GUI - quickstart 7545
3) скопировать аккаунты из GUI 0x7af6b57a0e4384501cfe9688778a405fe4c0de2ea8b96d0bb1216fff2987522a
4) копируем приватный ключ .env
5) тест транзакции ETH ✅, закрываем терминал

6) 
docker-compose up --build

7) запускаем ETH отдельно чтобы он связался с ganache
cd ~/desktop/server_loop/src/js/eth
ls
node api_eth.js

eth
http://localhost:3000/api/mywallet
btc
http://localhost:3001/api/mywallet
eth docker
http://localhost:3002/api/mywallet
trx
http://localhost:3003/api/mywallet

8) прописываем адреса 
cd ~/desktop/server_loop/src/php/loop_sender/public
walet_loop.php

$wallet_types=[
    "ETH"=>['api'=>"http://localhost:3000/api/send",],     
    "BTC"=>['api'=>"http://temp_backend:3001/api/send"],     
    "USDT"=>['api'=>"http://temp_backend:3003/api/send_usdt"],
];


$wallet_types=[
    "ETH"=>['api'=>"http://localhost:3000/api/send",],//"http://web3eth_api:3000/api/send",],     
    "BTC"=>['api'=>"http://bitbtc_api:3000/api/send"],     
    "USDT"=>['api'=>"http://tronweb_api:3000/api/send_usdt"],
];

📌📌📌 (проверить)
$wallet_types=[
    "ETH"=>['api'=>"http://web3eth_api:3000/api/send",],//"http://web3eth_api:3000/api/send",],     
    "BTC"=>['api'=>"http://bitbtc_api:3000/api/send"],     
    "USDT"=>['api'=>"http://tronweb_api:3000/api/send_usdt"],
];


function get_conn($sql=null,$vars=null){global $db;
    if(!$db) $db = pg_connect("host=116.202.103 dbname= port=5432 user= password=");
    return $db;
}

8)
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ganache_cli
- узнать ip ganache (📌работает только по ip)

9)
📌📌📌
cd ~/desktop/server_loop/src/js/eth

"localNodeGanache": "http://172.19.0.5:8545", 

- вписать важно http а не https

10) перезапуск 

docker-compose restart web3eth_api

//(другие при тестировании)
docker-compose restart bitbtc_api
docker-compose restart tronweb_api

11)
http://localhost:7777/
http://localhost:7777/wallet_loop.php

done

(доп)
#10) проверка коннекта - не нужно

docker exec -it loop_coins_php bash

#10) подключение к внешним ip host (docker) - не решено

12)
- 📌 не перезапускать ganache_cli (пересоздает кошельки)

12+1)

git pull
whoami

chmod -R 0755 wallet_loop.php 
exec -it loop_wallet_php bash

docker-compose build loop_coins_php
docker restart loop_coins_php
RUN mkdir /var/www/src/public/logs && chmod -R 775 /var/www/src/public/logs

cd /deploy/server_loop_temp

chmod -R 777 logs


#!/bin/bash
#docker exec -it loop_coins_php bash
#curl -s http://localhost/

http://localhost/run.php